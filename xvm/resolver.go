/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

package xvm

import (
	"fmt"

	"chainmaker.org/chainmaker/common/v2/serialize"
	"chainmaker.org/chainmaker/vm-wxvm/v2/xvm/exec"
)

const (
	contextIDKey = "ctxid"
	responseKey  = "callResponse"
)

type responseDesc struct {
	Body  []byte
	Error bool
}

type contextServiceResolver struct {
	contextService *ContextService
}

func NewContextServiceResolver(service *ContextService) exec.Resolver {
	return &contextServiceResolver{
		contextService: service,
	}
}

func (s *contextServiceResolver) ResolveGlobal(module, name string) (int64, bool) {
	return 0, false
}

func (s *contextServiceResolver) ResolveFunc(module, name string) (interface{}, bool) {
	fullname := module + "." + name
	switch fullname {
	case "env._call_method":
		return s.cCallMethod, true
	case "env._fetch_response":
		return s.cFetchResponse, true
	default:
		return nil, false
	}
}
func (s *contextServiceResolver) cFetchResponse(ctx exec.Context, userBuf, userLen uint32) uint32 {
	codec := exec.NewCodec(ctx)
	iresponse := ctx.GetUserData(responseKey)
	if iresponse == nil {
		exec.Throw(exec.NewTrap("call fetchResponse on nil value"))
	}
	response, ok := iresponse.(responseDesc)
	if !ok {
		exec.Throw(exec.NewTrap("call fetchResponse error"))
	}

	userbuf := codec.Bytes(userBuf, userLen)
	if len(response.Body) != len(userbuf) {
		exec.Throw(exec.NewTrap(fmt.Sprintf("call fetchResponse with bad length, got %d, expect %d",
			len(userbuf), len(response.Body))))
	}
	copy(userbuf, response.Body)
	success := uint32(1)
	if response.Error {
		success = 0
	}
	ctx.SetUserData(responseKey, nil)
	return success
}

func (s *contextServiceResolver) cCallMethod(
	ctx exec.Context,
	methodAddr, methodLen uint32,
	requestAddr, requestLen uint32,
	responseAddr, responseLen uint32,
	successAddr uint32) uint32 {

	codec := exec.NewCodec(ctx)
	ctxId, ok := ctx.GetUserData(contextIDKey).(int64)
	if !ok {
		s.contextService.logger.Error("convert user data to ctxId failed")
	}

	method := codec.String(methodAddr, methodLen)
	requestBufTmp := codec.Bytes(requestAddr, requestLen)
	responseBuf := codec.Bytes(responseAddr, responseLen)
	requestBuf := make([]byte, len(requestBufTmp))
	copy(requestBuf, requestBufTmp)

	context, ok := s.contextService.Context(ctxId)
	if !ok {
		s.contextService.logger.Error("encounter bad ctx id. failed to call method:", method)
		codec.SetUint32(successAddr, 1)
		return uint32(0)
	}

	reqItems := serialize.NewEasyCodecWithBytes(requestBuf).GetItems()
	//	s.contextService.ctxId = ctxId
	context.in = reqItems
	context.requestBody = requestBuf
	context.gasUsed = ctx.GasUsed()
	switch method {
	case "GetObject":
		s.contextService.GetState(ctxId)
	case "PutObject":
		s.contextService.PutState(ctxId)
	case "DeleteObject":
		s.contextService.DeleteState(ctxId)
	case "NewIterator":
		s.contextService.NewIterator(ctxId)
	case "GetCallArgs":
		s.contextService.GetCallArgs(ctxId)
	case "SetOutput":
		s.contextService.SetOutput(ctxId)
	case "ContractCall":
		s.contextService.CallContract(ctxId)
	case "LogMsg":
		s.contextService.LogMessage(ctxId)
	case "EmitEvent":
		s.contextService.EmitEvent(ctxId)
	default:
		s.contextService.logger.Error("no such method ", method)
	}
	if context.err != nil {
		s.contextService.logger.Errorw("failed to call method:", method, context.err)
		codec.SetUint32(successAddr, 1)
		return uint32(0)
	}

	possibleResponseBuf := serialize.NewEasyCodecWithItems(context.resp).Marshal()

	// fast path
	if len(possibleResponseBuf) <= len(responseBuf) {
		copy(responseBuf, possibleResponseBuf)
		codec.SetUint32(successAddr, 1)
		return uint32(len(possibleResponseBuf))
	}

	// slow path
	var resDesc responseDesc
	resDesc.Body = possibleResponseBuf
	ctx.SetUserData(responseKey, resDesc)
	return uint32(len(resDesc.Body))
}
